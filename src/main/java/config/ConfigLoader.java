package config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URLDecoder;
import java.util.Properties;

public class ConfigLoader {
    public static final String fileName = "server-config.properties";
    private static Properties properties = null;

    static {
        ConfigLoader scl = new ConfigLoader();
        scl.loadProperties();
    }

    private ConfigLoader() {
        properties = new Properties();
    }

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }

    public static void setProperty(String key, String value) {
        properties.setProperty(key, value);
    }

    public static void saveConfig() {
        try {
            ClassLoader classLoader = ConfigLoader.class.getClassLoader();
            String fileLocation = classLoader.getResource(fileName).getFile();
            File file = new File(URLDecoder.decode(fileLocation, "UTF-8"));
            FileOutputStream out = new FileOutputStream(file);
            properties.store(out, "Server config");
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadProperties() {
        try {
            ClassLoader classLoader = ConfigLoader.class.getClassLoader();
            String fileLocation = classLoader.getResource(fileName).getFile();
            File file = new File(URLDecoder.decode(fileLocation, "UTF-8"));
            FileInputStream in = new FileInputStream(file);
            properties.load(in);
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
