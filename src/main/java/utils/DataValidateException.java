package utils;

public class DataValidateException extends Exception {

    public DataValidateException() {
        super();
    }

    public DataValidateException(String message) {
        super(message);
    }
}
