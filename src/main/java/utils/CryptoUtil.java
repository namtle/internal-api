package utils;

import com.Ostermiller.util.Base64;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class CryptoUtil {
    public CryptoUtil() {
    }

    public static String encrypt(String input) throws Exception {
        Key key = new SecretKeySpec("cOol=ReVEgYwiTH.NEt&jAVa".getBytes("UTF-8"), "DESede");
        Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] inputBytes = input.getBytes();
        return Base64.encodeToString(cipher.doFinal(inputBytes));
    }

    public static String decrypt(String encryptedString) throws Exception {
        Key key = new SecretKeySpec("cOol=ReVEgYwiTH.NEt&jAVa".getBytes("UTF-8"), "DESede");
        Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] encryptedBytes = Base64.decodeToBytes(encryptedString);
        byte[] recoveredBytes = cipher.doFinal(encryptedBytes);
        return new String(recoveredBytes);
    }

    public static String decryptLocal(String encryptedString) throws Exception {
        Key key = new SecretKeySpec("l0c@lR3v3GEL0g1nSTorage!".getBytes("UTF-8"), "DESede");
        Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] encryptedBytes = Base64.decodeToBytes(encryptedString);
        byte[] recoveredBytes = cipher.doFinal(encryptedBytes);
        return new String(recoveredBytes);
    }

    public static String decryptAES(String cipherText, String encryptionKey, String iv) throws Exception {
        int len = cipherText.length();
        byte[] cipherBytes = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            cipherBytes[i / 2] = (byte) ((Character.digit(cipherText.charAt(i), 16) << 4)
                    + Character.digit(cipherText.charAt(i + 1), 16));
        }
        final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
        final SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv.getBytes("UTF-8")));
        return new String(cipher.doFinal(cipherBytes), "UTF-8");
    }
}
