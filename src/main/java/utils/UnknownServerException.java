package utils;

public class UnknownServerException extends RuntimeException {

    public UnknownServerException() {
        super();
    }

    public UnknownServerException(String message) {
        super(message);
    }

}
