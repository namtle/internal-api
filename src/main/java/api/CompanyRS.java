package api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import model.Company;
import service.CompanyService;

@Path("/companies")
@Api("Company APIs")
public class CompanyRS {

    @Context
    private HttpServletRequest request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get all company in system")
    public Response getAll() {
        try {
            List<Map<String, String>> rs = new ArrayList<>();
            List<Company> companyList = new CompanyService(request).getAllCompany();
            companyList.forEach(c -> {
                Map<String, String> tmp = new HashMap<>();
                tmp.put("id", c.getId());
                tmp.put("name", c.getName());
                tmp.put("v4enabled", c.isV4enabled() + "");
                rs.add(tmp);
            });
            return Response.ok(rs).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }

    @GET
    @Path("/{companyId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get company by its ID")
    public Response getCompanyById(@PathParam("companyId") String id) {
        try {
            return Response.ok(new CompanyService(request).getCompanyById(id)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }

    @POST
    @Path("/set-v4enabled")
    @Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
    @ApiOperation(hidden = true, value = "")
    public Response setV4Enabled(@ApiParam(required = true) @FormParam("companyId") String companyId,
            @FormParam("v4Enabled") boolean v4Enabled) {
        try {
            new CompanyService(request).setV4Enabled(companyId, v4Enabled);
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
        return Response.ok("Successfully").type(MediaType.TEXT_PLAIN).build();
    }
}
