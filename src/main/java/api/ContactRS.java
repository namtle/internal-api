package api;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import service.ContactService;

@Path("/contacts")
public class ContactRS {

    @Context
    HttpServletRequest request;

    @GET
    @Path("/{companyID}")
    public Response getContactListOfCompany(@PathParam("companyID") String companyID) {
        try {
            return Response.ok(new ContactService(request).getUnassignedContacts(companyID))
                    .type(MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }
}
