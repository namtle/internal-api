package api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import model.Login;
import service.LoginService;
import utils.UnknownServerException;

@Path("/login-info")
@Api("Login APIs")
public class LoginRS {

    @Context
    private HttpServletRequest request;

    @GET
    @Path("/loginName/{loginName}")
    public Response getInfo(@PathParam("loginName") String username) {
        List<Map<String, String>> output = new ArrayList<>();
        try {
            output = new LoginService(request).getLoginInfo(username).stream().map(login -> {
                Map<String, String> loginInfo = new HashMap<>();
                loginInfo.put("id", login.getId());
                loginInfo.put("loginName", login.getLoginName());
                loginInfo.put("password", login.getPassword());
                loginInfo.put("companyName", login.getCompanyName());
                return loginInfo;
            }).collect(Collectors.toList());
            return Response.ok(output).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }

    @GET
    @Path("/login/{name}")
    @ApiOperation(hidden = true, value = "")
    public Response getInfoByID(@PathParam("name") String name) {
        try {
            List<Login> rs = new LoginService(request).getLoginInfo(name);
            if (rs == null || (rs != null && rs.isEmpty())) {
                return Response.ok("Not found").type(MediaType.TEXT_PLAIN).build();
            } else
                return Response.ok(rs).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }

    @POST
    @Path("/create")
    @Produces(value = { MediaType.TEXT_PLAIN })
    @ApiOperation(hidden = true, value = "")
    public Response createLogin(NewUserModel body) {
        String newId = null;
        try {
            Login newLogin = body.login;
            newLogin.setActive(true);
            String userGroupId = body.userGroupId;
            int membershipType = body.membershipType;
            String teamContactKey = body.teamContactKey;
            if (userGroupId.trim().equals("0")) {
                userGroupId = null;
            }
            newId = new LoginService(request).addNewLogin(newLogin, userGroupId, membershipType, teamContactKey);
        } catch (Exception e) {
            return Response.status(500).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
        return Response.ok("Action successfully! This is new login ID: " + newId).type(MediaType.TEXT_PLAIN).build();
    }

    @POST
    @Path("/update")
    @Produces(value = { MediaType.TEXT_PLAIN })
    @ApiOperation(hidden = true, value = "")
    public Response updateLogin(@FormParam("loginID") String loginID,
            @FormParam("accountEnabled") boolean accountEnabled,
            @FormParam("opportunityEnabled") boolean opportunityEnabled,
            @FormParam("partnerEnabled") boolean partnerEnabled,
            @FormParam("portfolioEnabled") boolean portfolioEnabled, @FormParam("type") int type,
            @FormParam("accessOverride") int accessOverride) {
        Login newLogin = new Login();
        newLogin.setId(loginID);
        newLogin.setAccountEnabled(accountEnabled);
        newLogin.setOpportunityEnabled(opportunityEnabled);
        newLogin.setPartnerEnabled(partnerEnabled);
        newLogin.setPortfolioEnabled(portfolioEnabled);
        newLogin.setType(type);
        newLogin.setAccessOverride(accessOverride);
        try {
            new LoginService(request).update(newLogin);
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
        return Response.ok("Successfully").build();
    }

}

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
class NewUserModel {
    public Login login;
    public String userGroupId;
    public int membershipType;
    public String teamContactKey;

}
