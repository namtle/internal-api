package api;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import service.LoginRoleService;

@Path("login-roles")
public class LoginRoleRS {

    @Context
    HttpServletRequest request;

    @GET
    @Path("{companyID}")
    public Response getLoginRoleList(@PathParam("companyID") String companyID) {
        try {
            return Response.ok(new LoginRoleService(request).getList(companyID)).type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }
}
