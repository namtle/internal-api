package service;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import dao.LicenseDAO;
import dao.LoginDAO;
import model.License;
import model.Login;
import utils.CryptoUtil;

import java.util.Date;

public class LicenseService extends BaseDBService {

    public LicenseService(HttpServletRequest request) {
        super(request);
    }

    public LicenseService(String currentDB) {
        super(currentDB);
    }

    License getRecursive(String companyId) throws Exception {
        LicenseDAO lDao = new LicenseDAO(currentDB);
        License license = null;
        try {
            license = lDao.getByCompanyId(companyId);
        } catch (Exception e) {
            license = null;
        }

        if ((license == null) || license.getID().equals("")) {
            Login login = new LoginDAO(currentDB).getSystemAdmin();
            List<License> licenses = lDao.list();
            for (License lElem : licenses) {
                if ((login != null) && (login.getCompanyId() != null) && (!login.getCompanyId().equals(""))
                        && (login.getCompanyId().equals(lElem.getCompanyID()))) {
                    license = lDao.getByCompanyId(lElem.getCompanyID());
                    break;
                }
            }
        }

        if ((license == null) || license.getID().equals("")) {
            throw new Exception(
                    "Cannot find a valid license for your company.  Please contact your Company Administrator.");
        }

        setReadOnlyValues(license);
        return license;
    }

    private void setReadOnlyValues(License license) throws Exception {
        String decryptedString = CryptoUtil.decrypt(license.getLicenseKey());
        // System.out.println("Decrypted String= "+ decryptedString);
        String[] parsedString = decryptedString.split("\\|");
        int attrFound = 0;
        for (int i = 0; i < parsedString.length; i++) {
            String[] nVp = parsedString[i].split("=");
            if (nVp.length == 2) {
                if (nVp[0].equals("QTY")) {
                    license.setNumberOfLicense(new Integer(nVp[1]).intValue());
                    attrFound++;
                } else if (nVp[0].equals("CID")) {
                    license.setCompanyID(nVp[1]);
                    attrFound++;
                } else if (nVp[0].equals("ED")) {
                    SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy h:mm:ss a");
                    Date tempTime = sdf.parse(nVp[1]);
                    license.setExpirationDate(tempTime);
                    attrFound++;
                }
            }
        }
        if (attrFound != 3) {
            throw new Exception("The license key is invalid");
        }
    }
}
