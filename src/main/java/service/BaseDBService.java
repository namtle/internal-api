package service;

import javax.servlet.http.HttpServletRequest;

public class BaseDBService {
    String currentDB;
    static String CURRENT_DB = "current_DB";

    public BaseDBService() {

    }

    public BaseDBService(String currentDB) {
        this.currentDB = currentDB;
    }

    public BaseDBService(HttpServletRequest request) {
        currentDB = (String) request.getSession().getAttribute(CURRENT_DB);
    }
}
