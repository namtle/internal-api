package service;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import dao.ReportingGroupDAO;
import model.ReportingGroup;
import model.ReportingGroupGridConfig;
import model.ReportingGroupMapConfig;
import model.ReportingGroupMapTemplate;
import model.ReportingGroupMembership;
import model.ReportingGroupPlanConfig;
import model.ReportingGroupPlaybookConfig;
import utils.DataValidateException;

public class ReportingGroupService extends BaseDBService {

    public ReportingGroupService(HttpServletRequest request) {
        super(request);
    }

    public ReportingGroupService(String currentDB) {
        super(currentDB);
    }

    public List<ReportingGroup> getList(String companyID) throws Exception {
        return new ReportingGroupDAO(currentDB).listByCompanyID(companyID);
    }

    public ReportingGroup getShallowGroup(String ID) throws Exception {
        // because the LoginReportingGroup has been removed, we can just call the
        // regular get
        return this.get(ID);
    }

    public ReportingGroup get(String id) throws Exception {
        ReportingGroup rg = new ReportingGroupDAO(currentDB).get(id);
        if (rg == null) {
            System.err.println("No reporting group found for " + id);
            throw new DataValidateException("Unable to load reporting group");
        }
        return rg;
    }

    public ReportingGroup update(ReportingGroup reportingGroup) throws Exception {
        validate(reportingGroup);
        ReportingGroupDAO reportingGroupDao = new ReportingGroupDAO(currentDB);
        ReportingGroup currentReportingGroup = reportingGroupDao.get(reportingGroup.getID());
        currentReportingGroup.setCompanyID(reportingGroup.getCompanyID());
        currentReportingGroup.setGroupName(reportingGroup.getGroupName());
        currentReportingGroup.setParentGroupID(reportingGroup.getParentGroupID());
        currentReportingGroup.setPrimaryLoginID(reportingGroup.getPrimaryLoginID());
        currentReportingGroup.setIntegratedPluginKey(reportingGroup.getIntegratedPluginKey());

        updateGrids(currentReportingGroup, reportingGroup);
        updateMaps(currentReportingGroup, reportingGroup);
        updateMembership(currentReportingGroup, reportingGroup);
        updatePlans(currentReportingGroup, reportingGroup);
        updatePlaybooks(currentReportingGroup, reportingGroup);

        return reportingGroupDao.update(currentReportingGroup);
    }

    private void updatePlaybooks(ReportingGroup currentReportingGroup, ReportingGroup reportingGroup) {
        HashMap<String, ReportingGroupPlaybookConfig> currentPlaybooks = new HashMap<>();
        for (ReportingGroupPlaybookConfig p : currentReportingGroup.getReportingGroupPlaybookConfigSet()) {
            currentPlaybooks.put(p.getID(), p);
        }
        for (ReportingGroupPlaybookConfig p : reportingGroup.getReportingGroupPlaybookConfigSet()) {
            if (isNullOrBlank(p.getID())) {
                currentReportingGroup.getReportingGroupPlaybookConfigSet().add(p);
            } else if (currentPlaybooks.containsKey(p.getID())) {
                ReportingGroupPlaybookConfig cp = currentPlaybooks.get(p.getID());
                currentPlaybooks.remove(p.getID());
                cp.setPlaybookConfigKey(p.getPlaybookConfigKey());
            } else {
                p.setID(null);
                currentReportingGroup.getReportingGroupPlaybookConfigSet().add(p);
            }
        }
        for (ReportingGroupPlaybookConfig dp : currentPlaybooks.values()) {
            currentReportingGroup.getReportingGroupPlaybookConfigSet().remove(dp);
        }
    }

    private void updatePlans(ReportingGroup currentReportingGroup, ReportingGroup reportingGroup) {
        HashMap<String, ReportingGroupPlanConfig> currentPlans = new HashMap<>();
        for (ReportingGroupPlanConfig p : currentReportingGroup.getReportingGroupPlanConfigSet()) {
            currentPlans.put(p.getID(), p);
        }
        for (ReportingGroupPlanConfig p : reportingGroup.getReportingGroupPlanConfigSet()) {
            if (isNullOrBlank(p.getID())) {
                currentReportingGroup.getReportingGroupPlanConfigSet().add(p);
            } else if (currentPlans.containsKey(p.getID())) {
                ReportingGroupPlanConfig cp = currentPlans.get(p.getID());
                currentPlans.remove(p.getID());
                cp.setCrmPlan(p.isCrmPlan());
                cp.setIncludeInBriefingReport(p.isIncludeInBriefingReport());
                cp.setPlanConfigKey(p.getPlanConfigKey());
                cp.setRequired(p.isRequired());
            } else {
                p.setID(null);
                currentReportingGroup.getReportingGroupPlanConfigSet().add(p);
            }
        }
        for (ReportingGroupPlanConfig dp : currentPlans.values()) {
            currentReportingGroup.getReportingGroupPlanConfigSet().remove(dp);
        }
    }

    private void updateMembership(ReportingGroup currentReportingGroup, ReportingGroup reportingGroup) {
        HashMap<String, ReportingGroupMembership> currentMembership = new HashMap<>();
        for (ReportingGroupMembership m : currentReportingGroup.getReportingGroupMembershipSet()) {
            currentMembership.put(m.getID(), m);
        }
        for (ReportingGroupMembership m : reportingGroup.getReportingGroupMembershipSet()) {
            if (isNullOrBlank(m.getID())) {
                currentReportingGroup.getReportingGroupMembershipSet().add(m);
            } else if (currentMembership.containsKey(m.getID())) {
                ReportingGroupMembership cm = currentMembership.get(m.getID());
                currentMembership.remove(m.getID());
                cm.setLoginID(m.getLoginID());
                cm.setType(m.getType());
            } else {
                m.setID(null);
                currentReportingGroup.getReportingGroupMembershipSet().add(m);
            }
        }
        for (ReportingGroupMembership dm : currentMembership.values()) {
            currentReportingGroup.getReportingGroupMembershipSet().remove(dm);
        }
    }

    private void updateMaps(ReportingGroup currentReportingGroup, ReportingGroup reportingGroup) {
        HashMap<String, ReportingGroupMapConfig> currentMaps = new HashMap<>();
        for (ReportingGroupMapConfig m : currentReportingGroup.getReportingGroupMapConfigSet()) {
            currentMaps.put(m.getID(), m);
        }
        for (ReportingGroupMapConfig m : reportingGroup.getReportingGroupMapConfigSet()) {
            if (isNullOrBlank(m.getID())) {
                currentReportingGroup.getReportingGroupMapConfigSet().add(m);
            } else if (currentMaps.containsKey(m.getID())) {
                ReportingGroupMapConfig cm = currentMaps.get(m.getID());
                currentMaps.remove(m.getID());
                cm.setAccountActive(m.isAccountActive());
                cm.setAccountRequired(m.isAccountRequired());
                cm.setMapConfigKey(m.getMapConfigKey());
                cm.setOpportunityActive(m.isOpportunityActive());
                cm.setOpportunityRequired(m.isOpportunityRequired());
                cm.setPartnerActive(m.isPartnerActive());
                cm.setPartnerRequired(m.isPartnerRequired());
                updateMapTemplates(cm, m);
            } else {
                m.setID(null);
                currentReportingGroup.getReportingGroupMapConfigSet().add(m);
            }
        }
        for (ReportingGroupMapConfig dm : currentMaps.values()) {
            currentReportingGroup.getReportingGroupMapConfigSet().remove(dm);
        }
    }

    private void updateMapTemplates(ReportingGroupMapConfig currentReportingGroupMapConfig,
            ReportingGroupMapConfig reportingGroupMapConfig) {
        HashMap<String, ReportingGroupMapTemplate> currentTemplates = new HashMap<>();
        for (ReportingGroupMapTemplate t : currentReportingGroupMapConfig.getReportingGroupMapTemplateSet()) {
            currentTemplates.put(t.getID(), t);
        }
        for (ReportingGroupMapTemplate t : reportingGroupMapConfig.getReportingGroupMapTemplateSet()) {
            if (isNullOrBlank(t.getID())) {
                currentReportingGroupMapConfig.getReportingGroupMapTemplateSet().add(t);
            } else if (currentTemplates.containsKey(t.getID())) {
                ReportingGroupMapTemplate ct = currentTemplates.get(t.getID());
                currentTemplates.remove(t.getID());
                ct.setAccountActive(t.isAccountActive());
                ct.setAccountRequired(t.isAccountRequired());
                ct.setMapTemplateKey(t.getMapTemplateKey());
                ct.setOpportunityActive(t.isOpportunityActive());
                ct.setOpportunityRequired(t.isOpportunityRequired());
                ct.setPartnerActive(t.isPartnerActive());
                ct.setPartnerRequired(t.isPartnerRequired());
            } else {
                t.setID(null);
                currentReportingGroupMapConfig.getReportingGroupMapTemplateSet().add(t);
            }
        }
        for (ReportingGroupMapTemplate dt : currentTemplates.values()) {
            currentReportingGroupMapConfig.getReportingGroupMapTemplateSet().remove(dt);
        }
    }

    protected boolean isNullOrBlank(String val) {
        if (val == null)
            return true;
        if (val.trim().equals(""))
            return true;
        return false;
    }

    private void updateGrids(ReportingGroup currentReportingGroup, ReportingGroup reportingGroup) {
        HashMap<String, ReportingGroupGridConfig> currentGrids = new HashMap<>();
        for (ReportingGroupGridConfig g : currentReportingGroup.getReportingGroupGridConfigSet()) {
            currentGrids.put(g.getID(), g);
        }
        for (ReportingGroupGridConfig g : reportingGroup.getReportingGroupGridConfigSet()) {
            if (isNullOrBlank(g.getID())) {
                currentReportingGroup.getReportingGroupGridConfigSet().add(g);
            } else if (currentGrids.containsKey(g.getID())) {
                ReportingGroupGridConfig cg = currentGrids.get(g.getID());
                currentGrids.remove(g.getID());
                cg.setGridConfigKey(g.getGridConfigKey());
            } else {
                g.setID(null);
                currentReportingGroup.getReportingGroupGridConfigSet().add(g);
            }
        }
        for (ReportingGroupGridConfig dg : currentGrids.values()) {
            currentReportingGroup.getReportingGroupGridConfigSet().remove(dg);
        }
    }

    private void validate(ReportingGroup reportingGroup) throws Exception {
        ReportingGroupDAO reportingGroupDao = new ReportingGroupDAO(currentDB);

        List<ReportingGroup> existingReportingGroups = reportingGroupDao
                .listByNameCompanyID(reportingGroup.getGroupName(), reportingGroup.getCompanyID());
        if (existingReportingGroups.size() > 0) {
            if (reportingGroup.getID() == null) {
                throw new DataValidateException("Cannot have duplicate names");
            }
            for (ReportingGroup existingReportingGroup : existingReportingGroups) {
                if (!reportingGroup.getID().equals(existingReportingGroup.getID())) {
                    throw new DataValidateException("Cannot have duplicate names");
                }
            }
        }
    }
}
