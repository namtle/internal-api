package service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import dao.CompanyDAO;
import model.Company;
import utils.UnknownServerException;

public class CompanyService extends BaseDBService {

    public CompanyService(HttpServletRequest request) {
        super(request);
    }

    public CompanyService(String currentDB) {
        super(currentDB);
    }

    public List<Company> getAllCompany() throws Exception {
        return new CompanyDAO(currentDB).getAllCompany();
    }

    public Company getCompanyById(String id) throws Exception {
        return new CompanyDAO(currentDB).getCompanyById(id);
    }

    public void setV4Enabled(String companyId, boolean v4Enabled) throws Exception {
        new CompanyDAO(currentDB).setV4Enabled(companyId, v4Enabled);
    }
}
