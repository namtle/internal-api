package service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import config.ServerConfigLoader;
import dao.DAO;
import model.DatabaseServer;
import utils.UnknownServerException;

public class DatabaseService extends BaseDBService {

    private HttpSession session;

    public DatabaseService(HttpServletRequest request) {
        this.session = request.getSession();
        currentDB = (String) request.getSession().getAttribute(CURRENT_DB);
    }

    private void addDatabaseServer(DatabaseServer ds) throws Exception {
        String host = createHostName(ds.getHost(), ds.getPort());
        String url = "jdbc:mysql://" + host.trim() + "/" + ds.getSchema() + "?useSSL=false&tinyInt1isBit=false";
        DAO.addDatabaseServer(ds.getDatabaseName(), url, ds.getUsername(), ds.getPassword());
    }

    private String createHostName(String ip, int port) {
        String host = null;
        if (port <= 65535 && port > 1024) {
            host = ip.trim() + ":" + port;
        } else {
            host = ip.trim();
        }
        return host;
    }

    public void switchDatabaseServer(String name) {
        if (ServerConfigLoader.isDatabaseExisted(name)) {
            DatabaseServer ds = ServerConfigLoader.getDatabase(name);
            if (!DAO.checkDatabaseExist(ds.getDatabaseName())) {
                try {
                    addDatabaseServer(ds);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        } else
            throw new UnknownServerException("Database not found!");
        session.setAttribute(CURRENT_DB, name);
        currentDB = name;
    }

    public DatabaseServer getCurrentDatabaseServer() {
        return ServerConfigLoader.getDatabase(currentDB);
    }

}
