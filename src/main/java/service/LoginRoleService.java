package service;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import dao.LoginRoleDAO;
import model.LoginRole;

public class LoginRoleService extends BaseDBService {

    public LoginRoleService(HttpServletRequest request) {
        super(request);
    }

    public LoginRoleService(String currentDB) {
        super(currentDB);
    }

    public List<LoginRole> getList(String companyID) throws Exception {
        return new LoginRoleDAO(currentDB).listByCompanyID(companyID);
    }
}
