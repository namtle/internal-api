package service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import dao.ContactTypeDAO;
import model.ContactType;

public class ContactTypeService extends BaseDBService {

    public ContactTypeService(HttpServletRequest request) {
        super(request);
    }

    public ContactTypeService(String currentDB) {
        super(currentDB);
    }

    public List<ContactType> getList(String companyID) throws Exception {
        return new ContactTypeDAO(currentDB).listByCompanyID(companyID);
    }
}
