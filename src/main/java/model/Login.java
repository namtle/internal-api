package model;
// default package

// Generated Jan 22, 2018 4:43:53 PM by Hibernate Tools 5.2.6.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;

/**
 * Login generated by hbm2java
 */
@Entity
@Table(name = "login", catalog = "revegyenterprise_4_0_qa")
@XmlRootElement(name = "login")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value = "Login")
public class Login implements java.io.Serializable {
    private String id;
    private String loginName;
    private String firstName;
    private String lastName;
    private String shortName;
    private String password;
    private String email;
    private String companyId;
    private String companyName;
    private Date passwordSetDateTime;
    private Integer type;
    private Boolean active = true;
    private String spellCheckLanguage = "en_US";
    private int defaultCurrencyKey = 0;
    private String shortDateFormat = "MM.DD.YYYY";
    private String shortTimeFormat = "L:NN A";
    private int reportOutputType = 0;
    private int reportPasswordMode = 0;
    private String pageUOMDisplay = "in";
    private float pageHeight = 11f;
    private float pageWidth = 8.5f;
    private float pageTopMargin = .25f;
    private float pageBottomMargin = .25f;
    private float pageLeftMargin = .25f;
    private float pageRightMargin = .25f;
    private float mapMinScale = .5f;
    private int sendTaskReminderDays = -1;
    private boolean bestPracticePromptEnabled = true;
    private boolean reassign = false;
    private boolean linkAllWBInfo = true;
    private String ownerUsrKey;
    private boolean accountEnabled;
    private boolean opportunityEnabled;
    private boolean partnerEnabled;
    private boolean portfolioEnabled;
    private boolean stsiIntegrationDisabled;
    private String webConnectionKey;
    private int accessOverride;
    private String reportPassword;
    private String sendTaskNotifications;
    private String sendTaskReminders;
    private String loginGroupID;
    private String title;
    private String phone;
    private String loginRoleKey;

    public Login() {
    }

    @Formula("(select c.Name from company c where c.ID=companyId)")
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "ID", unique = true, nullable = false)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "LoginName", length = 100)
    public String getLoginName() {
        return this.loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Column(name = "Password", length = 250)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "Email", length = 80)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "CompanyID", length = 36)
    public String getCompanyId() {
        return this.companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "PasswordSetDateTime", nullable = false, length = 19)
    public Date getPasswordSetDateTime() {
        return this.passwordSetDateTime;
    }

    public void setPasswordSetDateTime(Date passwordSetDateTime) {
        this.passwordSetDateTime = passwordSetDateTime;
    }

    @Column(name = "Type")
    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Column(name = "Active", columnDefinition = "tinyint(1)")
    public Boolean getActive() {
        return this.active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @JsonIgnore
    @Column(name = "SpellCheckLanguage", length = 10)
    public String getSpellCheckLanguage() {
        return spellCheckLanguage;
    }

    public void setSpellCheckLanguage(String spellCheckLanguage) {
        this.spellCheckLanguage = spellCheckLanguage;
    }

    @JsonIgnore
    @Column(name = "DefaultCurrencyKey", nullable = false)
    public int getDefaultCurrencyKey() {
        return defaultCurrencyKey;
    }

    public void setDefaultCurrencyKey(int defaultCurrencyKey) {
        this.defaultCurrencyKey = defaultCurrencyKey;
    }

    @JsonIgnore
    @Column(name = "ShortDateFormat", length = 20)
    public String getShortDateFormat() {
        return shortDateFormat;
    }

    public void setShortDateFormat(String shortDateFormat) {
        this.shortDateFormat = shortDateFormat;
    }

    @JsonIgnore
    @Column(name = "ShortTimeFormat", length = 20)
    public String getShortTimeFormat() {
        return shortTimeFormat;
    }

    public void setShortTimeFormat(String shortTimeFormat) {
        this.shortTimeFormat = shortTimeFormat;
    }

    @JsonIgnore
    @Column(name = "ReportOutputType", nullable = false)
    public int getReportOutputType() {
        return reportOutputType;
    }

    public void setReportOutputType(int reportOutputType) {
        this.reportOutputType = reportOutputType;
    }

    @JsonIgnore
    @Column(name = "ReportPasswordMode", nullable = false)
    public int getReportPasswordMode() {
        return reportPasswordMode;
    }

    public void setReportPasswordMode(int reportPasswordMode) {
        this.reportPasswordMode = reportPasswordMode;
    }

    @JsonIgnore
    @Column(name = "PageUOMDisplay", length = 2)
    public String getPageUOMDisplay() {
        return pageUOMDisplay;
    }

    public void setPageUOMDisplay(String pageUOMDisplay) {
        this.pageUOMDisplay = pageUOMDisplay;
    }

    @JsonIgnore
    @Column(name = "PageHeight", nullable = false, precision = 12, scale = 0)
    public float getPageHeight() {
        return pageHeight;
    }

    public void setPageHeight(float pageHeight) {
        this.pageHeight = pageHeight;
    }

    @JsonIgnore
    @Column(name = "PageWidth", nullable = false, precision = 12, scale = 0)
    public float getPageWidth() {
        return pageWidth;
    }

    public void setPageWidth(float pageWidth) {
        this.pageWidth = pageWidth;
    }

    @JsonIgnore
    @Column(name = "PageTopMargin", nullable = false, precision = 12, scale = 0)
    public float getPageTopMargin() {
        return pageTopMargin;
    }

    public void setPageTopMargin(float pageTopMargin) {
        this.pageTopMargin = pageTopMargin;
    }

    @JsonIgnore
    @Column(name = "PageBottomMargin", nullable = false, precision = 12, scale = 0)
    public float getPageBottomMargin() {
        return pageBottomMargin;
    }

    public void setPageBottomMargin(float pageBottomMargin) {
        this.pageBottomMargin = pageBottomMargin;
    }

    @JsonIgnore
    @Column(name = "PageLeftMargin", nullable = false, precision = 12, scale = 0)
    public float getPageLeftMargin() {
        return pageLeftMargin;
    }

    public void setPageLeftMargin(float pageLeftMargin) {
        this.pageLeftMargin = pageLeftMargin;
    }

    @JsonIgnore
    @Column(name = "PageRightMargin", nullable = false, precision = 12, scale = 0)
    public float getPageRightMargin() {
        return pageRightMargin;
    }

    public void setPageRightMargin(float pageRightMargin) {
        this.pageRightMargin = pageRightMargin;
    }

    @JsonIgnore
    @Column(name = "MapMinScale", nullable = false, precision = 12, scale = 0)
    public float getMapMinScale() {
        return mapMinScale;
    }

    public void setMapMinScale(float mapMinScale) {
        this.mapMinScale = mapMinScale;
    }

    @JsonIgnore
    @Column(name = "SendTaskReminderDays", nullable = false)
    public int getSendTaskReminderDays() {
        return sendTaskReminderDays;
    }

    public void setSendTaskReminderDays(int sendTaskReminderDays) {
        this.sendTaskReminderDays = sendTaskReminderDays;
    }

    @Column(name = "FirstName", length = 40)
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "LastName", length = 40)
    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "ShortName", length = 8)
    public String getShortName() {
        return this.shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @JsonIgnore
    @Column(name = "OwnerUsrKey", length = 36)
    public String getOwnerUsrKey() {
        return ownerUsrKey;
    }

    public void setOwnerUsrKey(String ownerUsrKey) {
        this.ownerUsrKey = ownerUsrKey;
    }

    @JsonIgnore
    @Column(name = "BestPracticePromptEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isBestPracticePromptEnabled() {
        return bestPracticePromptEnabled;
    }

    public void setBestPracticePromptEnabled(boolean bestPracticePromptEnabled) {
        this.bestPracticePromptEnabled = bestPracticePromptEnabled;
    }

    @JsonIgnore
    @Column(name = "Reassign", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isReassign() {
        return reassign;
    }

    public void setReassign(boolean reassign) {
        this.reassign = reassign;
    }

    @JsonIgnore
    @Column(name = "LinkAllWBInfo", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isLinkAllWBInfo() {
        return linkAllWBInfo;
    }

    public void setLinkAllWBInfo(boolean linkAllWBInfo) {
        this.linkAllWBInfo = linkAllWBInfo;
    }

    @Column(name = "AccountEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isAccountEnabled() {
        return accountEnabled;
    }

    public void setAccountEnabled(boolean accountEnabled) {
        this.accountEnabled = accountEnabled;
    }

    @Column(name = "OpportunityEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isOpportunityEnabled() {
        return opportunityEnabled;
    }

    public void setOpportunityEnabled(boolean opportunityEnabled) {
        this.opportunityEnabled = opportunityEnabled;
    }

    @Column(name = "PartnerEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isPartnerEnabled() {
        return partnerEnabled;
    }

    public void setPartnerEnabled(boolean partnerEnabled) {
        this.partnerEnabled = partnerEnabled;
    }

    @Column(name = "PortfolioEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isPortfolioEnabled() {
        return portfolioEnabled;
    }

    public void setPortfolioEnabled(boolean portfolioEnabled) {
        this.portfolioEnabled = portfolioEnabled;
    }

    @JsonIgnore
    @Column(name = "StsiIntegrationDisabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isStsiIntegrationDisabled() {
        return stsiIntegrationDisabled;
    }

    public void setStsiIntegrationDisabled(boolean stsiIntegrationDisabled) {
        this.stsiIntegrationDisabled = stsiIntegrationDisabled;
    }

    @JsonIgnore
    @Column(name = "WebConnectionKey", length = 36)
    public String getWebConnectionKey() {
        return webConnectionKey;
    }

    public void setWebConnectionKey(String webConnectionKey) {
        this.webConnectionKey = webConnectionKey;
    }

    @Column(name = "AccessOverride", nullable = false)
    public int getAccessOverride() {
        return this.accessOverride;
    }

    public void setAccessOverride(int accessOverride) {
        this.accessOverride = accessOverride;
    }

    @JsonIgnore
    @Column(name = "SendTaskNotifications", length = 1)
    public String getSendTaskNotifications() {
        return sendTaskNotifications;
    }

    public void setSendTaskNotifications(String sendTaskNotifications) {
        this.sendTaskNotifications = sendTaskNotifications;
    }

    @JsonIgnore
    @Column(name = "SendTaskReminders", length = 1)
    public String getSendTaskReminders() {
        return sendTaskReminders;
    }

    public void setSendTaskReminders(String sendTaskReminders) {
        this.sendTaskReminders = sendTaskReminders;
    }

    @JsonIgnore
    @Column(name = "LoginGroupID", length = 36)
    public String getLoginGroupID() {
        return loginGroupID;
    }

    public void setLoginGroupID(String loginGroupID) {
        this.loginGroupID = loginGroupID;
    }

    @JsonIgnore
    @Column(name = "Title", length = 100)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonIgnore
    @Column(name = "Phone", length = 24)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "LoginRoleKey", length = 36)
    public String getLoginRoleKey() {
        return loginRoleKey;
    }

    public void setLoginRoleKey(String loginRoleKey) {
        this.loginRoleKey = loginRoleKey;
    }

}
