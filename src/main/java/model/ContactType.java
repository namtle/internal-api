package model;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@Entity
@Table(name = "contacttype", catalog = "revegyenterprise_4_0_qa")
public class ContactType implements Serializable {
    public ContactType() {
    }

    private ContacttypeId id;
    private String name;
    private String code;
    private int initValue = 0;
    private int sortOrder;

    @EmbeddedId

    @AttributeOverrides({
            @AttributeOverride(name = "companyId", column = @Column(name = "CompanyID", nullable = false, length = 32)),
            @AttributeOverride(name = "key", column = @Column(name = "\"Key\"", nullable = false, length = 36)) })
    public ContacttypeId getId() {
        return this.id;
    }

    public void setId(ContacttypeId id) {
        this.id = id;
    }

    @Column(name = "Name", length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "Code", length = 12)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "InitValue")
    public int getInitValue() {
        return initValue;
    }

    public void setInitValue(int initValue) {
        this.initValue = initValue;
    }

    @Column(name = "SortOrder")
    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public boolean equals(Object obj) {
        return (this == obj);
    }

    public int hashCode() {
        return super.hashCode();
    }
}
