package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ContacttypeId implements Serializable {
    private String companyId;
    private String key;

    public ContacttypeId() {
    }

    public ContacttypeId(String companyId, String key) {
        this.companyId = companyId;
        this.key = key;
    }

    @Column(name = "CompanyID", nullable = false, length = 32)
    public String getCompanyId() {
        return this.companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    @Column(name = "\"Key\"", nullable = false, length = 36)
    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof ContacttypeId))
            return false;
        ContacttypeId castOther = (ContacttypeId) other;

        return ((this.getCompanyId() == castOther.getCompanyId()) || (this.getCompanyId() != null
                && castOther.getCompanyId() != null && this.getCompanyId().equals(castOther.getCompanyId())))
                && ((this.getKey() == castOther.getKey()) || (this.getKey() != null && castOther.getKey() != null
                        && this.getKey().equals(castOther.getKey())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (getCompanyId() == null ? 0 : this.getCompanyId().hashCode());
        result = 37 * result + (getKey() == null ? 0 : this.getKey().hashCode());
        return result;
    }
}
