package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@Entity
@Table(name = "reportinggroupgridconfig", catalog = "revegyenterprise_4_0_qa")
public class ReportingGroupGridConfig implements Serializable {

    public ReportingGroupGridConfig() {
    }

    private String ID;
    private String gridConfigKey;
    private ReportingGroup reportinggroup;

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    @Column(name = "GridConfigKey")
    public String getGridConfigKey() {
        return gridConfigKey;
    }

    public void setGridConfigKey(String gridConfigKey) {
        this.gridConfigKey = gridConfigKey;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ReportingGroupID")
    public ReportingGroup getReportinggroup() {
        return this.reportinggroup;
    }

    public void setReportinggroup(ReportingGroup reportinggroup) {
        this.reportinggroup = reportinggroup;
    }

    public boolean equals(Object obj) {
        return (this == obj);
    }

    public int hashCode() {
        return super.hashCode();
    }

}
