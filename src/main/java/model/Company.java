package model;
// default package

// Generated Jan 22, 2018 4:43:53 PM by Hibernate Tools 5.2.6.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Company generated by hbm2java
 */
@Entity
@Table(name = "company", catalog = "revegyenterprise_4_0_qa")
@XmlRootElement(name = "company")
@XmlAccessorType(XmlAccessType.FIELD)
public class Company implements java.io.Serializable {

    private String id;
    private String name;
    private String referenceId;
    private String primaryContact;
    private String phone;
    private String email;
    private String comments;
    private Integer monthsToSaveInactiveAccounts;
    private Boolean allowWbtoCreateCompetitors;
    private Boolean allowWbtoCreateSolutions;
    private boolean allowWbtoCreateContacts;
    private boolean allowMultiCurrency;
    private boolean allowWbtoAddPartner;
    private boolean allowWbtoAddAccount;
    private boolean allowWbtoAddOpportunity;
    private boolean allowWbcheckoutOffline;
    private int editingTimeoutMinutes;
    private int passwordMinLength;
    private int passwordMaxLength;
    private boolean passwordRequiresUppercase;
    private boolean passwordRequiresLowercase;
    private boolean passwordRequiresNumeric;
    private boolean passwordRequiresSymbol;
    private int passwordExpirationDays;
    private int passwordCannotMatchPrevious;
    private int allowDuplicatePartner;
    private int allowDuplicateAccount;
    private int allowDuplicateOpportunity;
    private String embeddedCrmplugInKey;
    private String defaultBackColor;
    private String defaultTextColor;
    private String accountBackColor;
    private String accountTextColor;
    private String opportunityBackColor;
    private String opportunityTextColor;
    private String partnerBackColor;
    private String partnerTextColor;
    private String stsiPlugInKey;
    private int monthFyends;
    private boolean createInfluencersEnabled;
    private String createInfluencerOverrideUrl;
    private boolean editInfluencersEnabled;
    private String editInfluencerOverrideUrl;
    private boolean v4enabled;

    public Company() {
    }

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "Name", length = 80)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "ReferenceID", length = 80)
    public String getReferenceId() {
        return this.referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    @Column(name = "PrimaryContact", length = 80)
    public String getPrimaryContact() {
        return this.primaryContact;
    }

    public void setPrimaryContact(String primaryContact) {
        this.primaryContact = primaryContact;
    }

    @Column(name = "Phone", length = 24)
    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "Email", length = 80)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "Comments", length = 480)
    public String getComments() {
        return this.comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Column(name = "MonthsToSaveInactiveAccounts")
    public Integer getMonthsToSaveInactiveAccounts() {
        return this.monthsToSaveInactiveAccounts;
    }

    public void setMonthsToSaveInactiveAccounts(Integer monthsToSaveInactiveAccounts) {
        this.monthsToSaveInactiveAccounts = monthsToSaveInactiveAccounts;
    }

    @Column(name = "AllowWBToCreateCompetitors", columnDefinition = "tinyint(1)")
    public Boolean getAllowWbtoCreateCompetitors() {
        return this.allowWbtoCreateCompetitors;
    }

    public void setAllowWbtoCreateCompetitors(Boolean allowWbtoCreateCompetitors) {
        this.allowWbtoCreateCompetitors = allowWbtoCreateCompetitors;
    }

    @Column(name = "AllowWBToCreateSolutions", columnDefinition = "tinyint(1)")
    public Boolean getAllowWbtoCreateSolutions() {
        return this.allowWbtoCreateSolutions;
    }

    public void setAllowWbtoCreateSolutions(Boolean allowWbtoCreateSolutions) {
        this.allowWbtoCreateSolutions = allowWbtoCreateSolutions;
    }

    @Column(name = "AllowWBToCreateContacts", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isAllowWbtoCreateContacts() {
        return this.allowWbtoCreateContacts;
    }

    public void setAllowWbtoCreateContacts(boolean allowWbtoCreateContacts) {
        this.allowWbtoCreateContacts = allowWbtoCreateContacts;
    }

    @Column(name = "AllowMultiCurrency", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isAllowMultiCurrency() {
        return this.allowMultiCurrency;
    }

    public void setAllowMultiCurrency(boolean allowMultiCurrency) {
        this.allowMultiCurrency = allowMultiCurrency;
    }

    @Column(name = "AllowWBToAddPartner", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isAllowWbtoAddPartner() {
        return this.allowWbtoAddPartner;
    }

    public void setAllowWbtoAddPartner(boolean allowWbtoAddPartner) {
        this.allowWbtoAddPartner = allowWbtoAddPartner;
    }

    @Column(name = "AllowWBToAddAccount", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isAllowWbtoAddAccount() {
        return this.allowWbtoAddAccount;
    }

    public void setAllowWbtoAddAccount(boolean allowWbtoAddAccount) {
        this.allowWbtoAddAccount = allowWbtoAddAccount;
    }

    @Column(name = "AllowWBToAddOpportunity", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isAllowWbtoAddOpportunity() {
        return this.allowWbtoAddOpportunity;
    }

    public void setAllowWbtoAddOpportunity(boolean allowWbtoAddOpportunity) {
        this.allowWbtoAddOpportunity = allowWbtoAddOpportunity;
    }

    @Column(name = "AllowWBCheckoutOffline", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isAllowWbcheckoutOffline() {
        return this.allowWbcheckoutOffline;
    }

    public void setAllowWbcheckoutOffline(boolean allowWbcheckoutOffline) {
        this.allowWbcheckoutOffline = allowWbcheckoutOffline;
    }

    @Column(name = "EditingTimeoutMinutes", nullable = false)
    public int getEditingTimeoutMinutes() {
        return this.editingTimeoutMinutes;
    }

    public void setEditingTimeoutMinutes(int editingTimeoutMinutes) {
        this.editingTimeoutMinutes = editingTimeoutMinutes;
    }

    @Column(name = "PasswordMinLength", nullable = false)
    public int getPasswordMinLength() {
        return this.passwordMinLength;
    }

    public void setPasswordMinLength(int passwordMinLength) {
        this.passwordMinLength = passwordMinLength;
    }

    @Column(name = "PasswordMaxLength", nullable = false)
    public int getPasswordMaxLength() {
        return this.passwordMaxLength;
    }

    public void setPasswordMaxLength(int passwordMaxLength) {
        this.passwordMaxLength = passwordMaxLength;
    }

    @Column(name = "PasswordRequiresUppercase", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isPasswordRequiresUppercase() {
        return this.passwordRequiresUppercase;
    }

    public void setPasswordRequiresUppercase(boolean passwordRequiresUppercase) {
        this.passwordRequiresUppercase = passwordRequiresUppercase;
    }

    @Column(name = "PasswordRequiresLowercase", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isPasswordRequiresLowercase() {
        return this.passwordRequiresLowercase;
    }

    public void setPasswordRequiresLowercase(boolean passwordRequiresLowercase) {
        this.passwordRequiresLowercase = passwordRequiresLowercase;
    }

    @Column(name = "PasswordRequiresNumeric", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isPasswordRequiresNumeric() {
        return this.passwordRequiresNumeric;
    }

    public void setPasswordRequiresNumeric(boolean passwordRequiresNumeric) {
        this.passwordRequiresNumeric = passwordRequiresNumeric;
    }

    @Column(name = "PasswordRequiresSymbol", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isPasswordRequiresSymbol() {
        return this.passwordRequiresSymbol;
    }

    public void setPasswordRequiresSymbol(boolean passwordRequiresSymbol) {
        this.passwordRequiresSymbol = passwordRequiresSymbol;
    }

    @Column(name = "PasswordExpirationDays", nullable = false)
    public int getPasswordExpirationDays() {
        return this.passwordExpirationDays;
    }

    public void setPasswordExpirationDays(int passwordExpirationDays) {
        this.passwordExpirationDays = passwordExpirationDays;
    }

    @Column(name = "PasswordCannotMatchPrevious", nullable = false)
    public int getPasswordCannotMatchPrevious() {
        return this.passwordCannotMatchPrevious;
    }

    public void setPasswordCannotMatchPrevious(int passwordCannotMatchPrevious) {
        this.passwordCannotMatchPrevious = passwordCannotMatchPrevious;
    }

    @Column(name = "AllowDuplicatePartner", nullable = false)
    public int getAllowDuplicatePartner() {
        return this.allowDuplicatePartner;
    }

    public void setAllowDuplicatePartner(int allowDuplicatePartner) {
        this.allowDuplicatePartner = allowDuplicatePartner;
    }

    @Column(name = "AllowDuplicateAccount", nullable = false)
    public int getAllowDuplicateAccount() {
        return this.allowDuplicateAccount;
    }

    public void setAllowDuplicateAccount(int allowDuplicateAccount) {
        this.allowDuplicateAccount = allowDuplicateAccount;
    }

    @Column(name = "AllowDuplicateOpportunity", nullable = false)
    public int getAllowDuplicateOpportunity() {
        return this.allowDuplicateOpportunity;
    }

    public void setAllowDuplicateOpportunity(int allowDuplicateOpportunity) {
        this.allowDuplicateOpportunity = allowDuplicateOpportunity;
    }

    @Column(name = "EmbeddedCRMPlugInKey", length = 36)
    public String getEmbeddedCrmplugInKey() {
        return this.embeddedCrmplugInKey;
    }

    public void setEmbeddedCrmplugInKey(String embeddedCrmplugInKey) {
        this.embeddedCrmplugInKey = embeddedCrmplugInKey;
    }

    @Column(name = "DefaultBackColor")
    public String getDefaultBackColor() {
        return this.defaultBackColor;
    }

    public void setDefaultBackColor(String defaultBackColor) {
        this.defaultBackColor = defaultBackColor;
    }

    @Column(name = "DefaultTextColor")
    public String getDefaultTextColor() {
        return this.defaultTextColor;
    }

    public void setDefaultTextColor(String defaultTextColor) {
        this.defaultTextColor = defaultTextColor;
    }

    @Column(name = "AccountBackColor")
    public String getAccountBackColor() {
        return this.accountBackColor;
    }

    public void setAccountBackColor(String accountBackColor) {
        this.accountBackColor = accountBackColor;
    }

    @Column(name = "AccountTextColor")
    public String getAccountTextColor() {
        return this.accountTextColor;
    }

    public void setAccountTextColor(String accountTextColor) {
        this.accountTextColor = accountTextColor;
    }

    @Column(name = "OpportunityBackColor")
    public String getOpportunityBackColor() {
        return this.opportunityBackColor;
    }

    public void setOpportunityBackColor(String opportunityBackColor) {
        this.opportunityBackColor = opportunityBackColor;
    }

    @Column(name = "OpportunityTextColor")
    public String getOpportunityTextColor() {
        return this.opportunityTextColor;
    }

    public void setOpportunityTextColor(String opportunityTextColor) {
        this.opportunityTextColor = opportunityTextColor;
    }

    @Column(name = "PartnerBackColor")
    public String getPartnerBackColor() {
        return this.partnerBackColor;
    }

    public void setPartnerBackColor(String partnerBackColor) {
        this.partnerBackColor = partnerBackColor;
    }

    @Column(name = "PartnerTextColor")
    public String getPartnerTextColor() {
        return this.partnerTextColor;
    }

    public void setPartnerTextColor(String partnerTextColor) {
        this.partnerTextColor = partnerTextColor;
    }

    @Column(name = "StsiPlugInKey", length = 36)
    public String getStsiPlugInKey() {
        return this.stsiPlugInKey;
    }

    public void setStsiPlugInKey(String stsiPlugInKey) {
        this.stsiPlugInKey = stsiPlugInKey;
    }

    @Column(name = "MonthFYEnds", nullable = false)
    public int getMonthFyends() {
        return this.monthFyends;
    }

    public void setMonthFyends(int monthFyends) {
        this.monthFyends = monthFyends;
    }

    @Column(name = "CreateInfluencersEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isCreateInfluencersEnabled() {
        return this.createInfluencersEnabled;
    }

    public void setCreateInfluencersEnabled(boolean createInfluencersEnabled) {
        this.createInfluencersEnabled = createInfluencersEnabled;
    }

    @Column(name = "CreateInfluencerOverrideUrl", length = 500)
    public String getCreateInfluencerOverrideUrl() {
        return this.createInfluencerOverrideUrl;
    }

    public void setCreateInfluencerOverrideUrl(String createInfluencerOverrideUrl) {
        this.createInfluencerOverrideUrl = createInfluencerOverrideUrl;
    }

    @Column(name = "EditInfluencersEnabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isEditInfluencersEnabled() {
        return this.editInfluencersEnabled;
    }

    public void setEditInfluencersEnabled(boolean editInfluencersEnabled) {
        this.editInfluencersEnabled = editInfluencersEnabled;
    }

    @Column(name = "EditInfluencerOverrideUrl", length = 500)
    public String getEditInfluencerOverrideUrl() {
        return this.editInfluencerOverrideUrl;
    }

    public void setEditInfluencerOverrideUrl(String editInfluencerOverrideUrl) {
        this.editInfluencerOverrideUrl = editInfluencerOverrideUrl;
    }

    @Column(name = "V4Enabled", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isV4enabled() {
        return this.v4enabled;
    }

    public void setV4enabled(boolean v4enabled) {
        this.v4enabled = v4enabled;
    }

}
