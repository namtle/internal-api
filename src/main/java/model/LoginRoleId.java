package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class LoginRoleId implements java.io.Serializable {

    private String companyId;
    private String key;

    public LoginRoleId() {
    }

    public LoginRoleId(String companyId, String key) {
        this.companyId = companyId;
        this.key = key;
    }

    @Column(name = "CompanyID", nullable = false, length = 36)
    public String getCompanyId() {
        return this.companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    @Column(name = "Key", nullable = false, length = 36)
    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

}
