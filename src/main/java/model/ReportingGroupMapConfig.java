package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@Entity
@Table(name = "reportinggroupmapconfig", catalog = "revegyenterprise_4_0_qa")
public class ReportingGroupMapConfig implements Serializable {

    public ReportingGroupMapConfig() {
    }

    private ReportingGroup reportinggroup;
    private String ID;
    private String mapConfigKey;
    private boolean accountActive;
    private boolean accountRequired;
    private boolean partnerActive;
    private boolean partnerRequired;
    private boolean opportunityActive;
    private boolean opportunityRequired;
    private boolean portfolioActive;
    private boolean portfolioRequired;
    private Set<ReportingGroupMapTemplate> reportingGroupMapTemplateSet = new HashSet<>();

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    @Column(name = "MapConfigKey")
    public String getMapConfigKey() {
        return mapConfigKey;
    }

    public void setMapConfigKey(String mapConfigKey) {
        this.mapConfigKey = mapConfigKey;
    }

    @Column(name = "AccountActive", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isAccountActive() {
        return accountActive;
    }

    public void setAccountActive(boolean accountActive) {
        this.accountActive = accountActive;
    }

    @Column(name = "AccountRequired", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isAccountRequired() {
        return accountRequired;
    }

    public void setAccountRequired(boolean accountRequired) {
        this.accountRequired = accountRequired;
    }

    @Column(name = "PartnerActive", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isPartnerActive() {
        return partnerActive;
    }

    public void setPartnerActive(boolean partnerActive) {
        this.partnerActive = partnerActive;
    }

    @Column(name = "PartnerRequired", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isPartnerRequired() {
        return partnerRequired;
    }

    public void setPartnerRequired(boolean partnerRequired) {
        this.partnerRequired = partnerRequired;
    }

    @Column(name = "OpportunityActive", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isOpportunityActive() {
        return opportunityActive;
    }

    public void setOpportunityActive(boolean opportunityActive) {
        this.opportunityActive = opportunityActive;
    }

    @Column(name = "OpportunityRequired", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isOpportunityRequired() {
        return opportunityRequired;
    }

    public void setOpportunityRequired(boolean opportunityRequired) {
        this.opportunityRequired = opportunityRequired;
    }

    @Column(name = "PortfolioActive", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isPortfolioActive() {
        return portfolioActive;
    }

    public void setPortfolioActive(boolean portfolioActive) {
        this.portfolioActive = portfolioActive;
    }

    @Column(name = "PortfolioRequired", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isPortfolioRequired() {
        return portfolioRequired;
    }

    public void setPortfolioRequired(boolean portfolioRequired) {
        this.portfolioRequired = portfolioRequired;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "reportinggroupmapconfig")
    public Set<ReportingGroupMapTemplate> getReportingGroupMapTemplateSet() {
        return reportingGroupMapTemplateSet;
    }

    public void setReportingGroupMapTemplateSet(Set<ReportingGroupMapTemplate> reportingGroupMapTemplateSet) {
        this.reportingGroupMapTemplateSet = reportingGroupMapTemplateSet;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ReportingGroupID")
    public ReportingGroup getReportinggroup() {
        return this.reportinggroup;
    }

    public void setReportinggroup(ReportingGroup reportinggroup) {
        this.reportinggroup = reportinggroup;
    }

}
