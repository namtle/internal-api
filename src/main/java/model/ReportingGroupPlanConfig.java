package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@Entity
@Table(name = "reportinggroupplanconfig", catalog = "revegyenterprise_4_0_qa")
public class ReportingGroupPlanConfig implements Serializable {

    public ReportingGroupPlanConfig() {
    }

    private String ID;
    private String planConfigKey;
    private boolean required;
    private boolean includeInBriefingReport;
    private boolean crmPlan;
    private ReportingGroup reportinggroup;

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    @Column(name = "PlanConfigKey")
    public String getPlanConfigKey() {
        return planConfigKey;
    }

    public void setPlanConfigKey(String planConfigKey) {
        this.planConfigKey = planConfigKey;
    }

    @Column(name = "Required", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    @Column(name = "IncludeInBriefingReport", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isIncludeInBriefingReport() {
        return includeInBriefingReport;
    }

    public void setIncludeInBriefingReport(boolean includeInBriefingReport) {
        this.includeInBriefingReport = includeInBriefingReport;
    }

    @Column(name = "CrmPlan", nullable = false, columnDefinition = "tinyint(1)")
    public boolean isCrmPlan() {
        return crmPlan;
    }

    public void setCrmPlan(boolean crmPlan) {
        this.crmPlan = crmPlan;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ReportingGroupID")
    public ReportingGroup getReportinggroup() {
        return this.reportinggroup;
    }

    public void setReportinggroup(ReportingGroup reportinggroup) {
        this.reportinggroup = reportinggroup;
    }

}
