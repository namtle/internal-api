package dao;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import model.Account;
import model.Company;
import model.Contact;
import model.ContactId;
import model.ContactType;
import model.ContacttypeId;
import model.License;
import model.Login;
import model.LoginRole;
import model.LoginRoleId;
import model.ReportingGroup;
import model.ReportingGroupGridConfig;
import model.ReportingGroupMapConfig;
import model.ReportingGroupMapTemplate;
import model.ReportingGroupMembership;
import model.ReportingGroupPlanConfig;
import model.ReportingGroupPlaybookConfig;
import utils.UnknownServerException;

public class DAO {
    Session session = null;
    private static final Map<String, SessionFactory> sessionFactoryHolder;
    private String currentDatabase;
    static {
        sessionFactoryHolder = new HashMap<>();
    }

    public DAO(String currentDatabase) {
        this.currentDatabase = currentDatabase;
    }

    void closeSession() {
        if (session != null) {
            session.close();
            session = null;
        }
    }

    Session getSession() {
        if (session == null)
            session = getSessionFactory().openSession();
        return session;
    }

    Transaction begin() {
        return getSession().beginTransaction();
    }

    void commit() {
        getSession().getTransaction().commit();
    }

    void rollback() {
        getSession().getTransaction().rollback();
    }

    public static boolean checkDatabaseExist(String databaseName) {
        return sessionFactoryHolder.get(databaseName) != null;
    }

    public static void addDatabaseServer(String name, String url, String username, String password) throws Exception {
        if (!checkDatabaseExist(name)) {
            SessionFactory sessionFactory = buildSessionFactory(url, username, password);
            if (sessionFactory == null) {
                throw new Exception("Can not get sessionFactory");
            }
            sessionFactoryHolder.put(name, sessionFactory);
        } else
            throw new Exception("This database name is already exist");
    }

    private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            return new Configuration().configure("/hibernate.cfg.xml").buildSessionFactory();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    private static Class[] HIBERNATE_MAP_CLASSES = { Account.class, Company.class, Contact.class, ContactId.class,
            ContactType.class, ContacttypeId.class, License.class, Login.class, LoginRole.class, LoginRoleId.class,
            ReportingGroupGridConfig.class, ReportingGroup.class, ReportingGroupMapConfig.class,
            ReportingGroupMapTemplate.class, ReportingGroupMembership.class, ReportingGroupPlanConfig.class,
            ReportingGroupPlaybookConfig.class };

    private static SessionFactory buildSessionFactory(String url, String username, String password) {
        // Create the SessionFactory from hibernate.cfg.xml
        Configuration configuration = new Configuration();
        configuration.configure();
        configuration.setProperty("hibernate.connection.url", url);
        configuration.setProperty("hibernate.connection.username", username);
        configuration.setProperty("hibernate.connection.password", password);
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        Arrays.asList(HIBERNATE_MAP_CLASSES).stream().forEach(configuration::addAnnotatedClass);
        return configuration.buildSessionFactory(serviceRegistry);
    }

    public SessionFactory getSessionFactory() {
        SessionFactory sessionFactory = sessionFactoryHolder.get(currentDatabase);
        if (sessionFactory == null) {
            throw new UnknownServerException("Please choose server first");
        }
        return sessionFactory;
    }

}
