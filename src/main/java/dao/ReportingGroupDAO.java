package dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import model.ReportingGroup;

public final class ReportingGroupDAO extends DAO {
    public ReportingGroupDAO(String currentDatabase) {
        super(currentDatabase);
    }

    public ReportingGroup update(ReportingGroup reportingGroup) throws Exception {
        try {
            begin();
            getSession().update(reportingGroup);
            commit();
            return reportingGroup;
        } catch (Exception ex) {
            rollback();
            throw ex;
        } finally {
            closeSession();
        }
    }

    public List<ReportingGroup> listByNameCompanyID(String groupName, String companyID) throws Exception {
        try {
            DetachedCriteria groups = DetachedCriteria.forClass(ReportingGroup.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.ilike("groupName", groupName, MatchMode.EXACT))
                    .add(Restrictions.eq("companyID", companyID)).addOrder(Order.asc("groupName"));
            return groups.getExecutableCriteria(getSession()).list();
        } finally {
            closeSession();
        }
    }

    public List<ReportingGroup> listByCompanyID(String companyID) throws Exception {
        try {
            DetachedCriteria groups = DetachedCriteria.forClass(ReportingGroup.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).add(Restrictions.eq("companyID", companyID))
                    .addOrder(Order.asc("groupName"));
            return groups.getExecutableCriteria(getSession()).list();
        } finally {
            closeSession();
        }
    }

    public ReportingGroup get(String id) throws Exception {
        try {
            return (ReportingGroup) getSession().get(ReportingGroup.class, id);
        } finally {
            closeSession();
        }
    }

}
