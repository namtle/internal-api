package dao;

import java.util.List;

import model.Company;

public class CompanyDAO extends DAO {

    public CompanyDAO(String currentDatabase) {
        super(currentDatabase);
    }

    public Company getCompanyById(String id) throws Exception {
        Company rs = null;
        try {
            session = getSessionFactory().openSession();
            String HQL = "from Company where ID=:id";
            List<Company> rsList = session.createQuery(HQL).setParameter("id", id).list();
            if (rsList != null && !rsList.isEmpty()) {
                rs = rsList.get(0);
            }
        } finally {
            closeSession();
        }
        return rs;
    }

    public List<Company> getAllCompany() throws Exception {
        List<Company> rs = null;
        try {
            session = getSessionFactory().openSession();
            String HQL = "from Company c";
            rs = session.createQuery(HQL).list();
        } finally {
            closeSession();
        }
        return rs;
    }

    public void setV4Enabled(String id, boolean enabled) throws Exception {
        try {
            session = getSessionFactory().openSession();
            session.beginTransaction();
            Company company = session.get(Company.class, id);
            company.setV4enabled(enabled);
            session.update(company);
            session.getTransaction().commit();
        } finally {
            closeSession();
        }
    }
}
