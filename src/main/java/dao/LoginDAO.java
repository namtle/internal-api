package dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import model.Login;

public class LoginDAO extends DAO {

    public LoginDAO(String currentDatabase) {
        super(currentDatabase);
    }

    public Login update(Login login) throws Exception {
        try {
            begin();
            getSession().update(login);
            commit();
            return login;
        } catch (Exception ex) {
            rollback();
            throw ex;
        } finally {
            closeSession();
        }
    }

    public List<Login> listByNameCompanyID(String loginName, String companyID) throws Exception {
        try {
            DetachedCriteria logins = DetachedCriteria.forClass(Login.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.ilike("loginName", loginName, MatchMode.EXACT))
                    .add(Restrictions.eq("companyId", companyID));
            return logins.getExecutableCriteria(getSessionFactory().openSession()).list();
        } finally {
            closeSession();
        }
    }

    public Login getLoginInfoById(String login_id) throws Exception {
        Login rs = null;
        try {
            String HQL = "from Login where id = :id";
            List<Login> rsList = getSession().createQuery(HQL).setParameter("id", login_id).getResultList();
            if (rsList != null && !rsList.isEmpty()) {
                rs = rsList.get(0);
            }
            return rs;
        } finally {
            closeSession();
        }
    }

    public List<Login> getLoginInfoByLoginname(String loginName) throws Exception {
        try {
            String HQL = "from Login where loginName like ?";
            return getSession().createQuery(HQL).setParameter(0, "%" + loginName + "%").getResultList();
        } finally {
            closeSession();
        }
    }

    public String createNewUSer(Login newUser) throws Exception {
        String newId = null;
        try {
            begin();
            newUser.setPasswordSetDateTime(new Date());
            getSession().save(newUser);
            newId = newUser.getId();
            commit();
        } catch (Exception e) {
            rollback();
            throw e;
        } finally {
            closeSession();
        }
        return newId;
    }

    public int getNumberOfLogins(String companyId) throws Exception {
        try {
            Long integer = (Long) getSessionFactory().openSession()
                    .createQuery("SELECT COUNT(*) FROM Login WHERE companyId='" + companyId + "'").uniqueResult();
            return integer.intValue();
        } finally {
            closeSession();
        }
    }

    public Login getSystemAdmin() throws Exception {
        try {
            DetachedCriteria logins = DetachedCriteria.forClass(Login.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).add(Restrictions.eq("type", 0));
            List<Login> list = logins.getExecutableCriteria(getSessionFactory().openSession()).list();
            if (list.isEmpty()) {
                throw new Exception("A System Admin Account does not exist");
            } else if (list.size() > 1) {
                throw new Exception("Multiple System Admins exist");
            }
            return list.get(0);
        } finally {
            closeSession();
        }
    }
}
