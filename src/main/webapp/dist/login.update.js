$(function() {
    $(getUpdateForm()).submit(function(e) {
        e.preventDefault();
    });
});
var loginList;
function getUpdateForm() {
    return $('#updateLoginForm');
}

function getLoginInfo() {
    var form = getUpdateForm();
    var alertTag = $(form).find('#rsStatus');
    var name = getFirstAttrByName(form, 'txtSearch');
    if (name.trim() === '') {
        alertTag.text('Please input loginID');
        alertTag.prop('color', 'red');
    } else
        $.ajax({
            url: getLogins_URL + name,
            type: 'GET',
            success: function(data) {
                if (typeof data !== 'string') {
                    alertTag.text('');
                    loginList = data;
                    var usernameList = $(getUpdateForm()).find('[name=loginID]').first();
                    usernameList.html('');
                    for (i = 0; i < loginList.length; i++) {
                        usernameList.append('<option value="' + loginList[i].id + '">' + loginList[i].loginName + '</option>');
                    }
                    onLoginNameChange();
                } else {
                    alertTag.text('Not found this login');
                    alertTag.prop('color', 'red');
                }
            }, error: function(response) {
                alertTag.text(response.responseText);
                alertTag.prop('color', 'red');
            }
        });
}

function onLoginNameChange() {
    var form = getUpdateForm();
    var loginID = getFirstAttrByName(form, 'loginID');
    for (i = 0; i < loginList.length; i++) {
        if (loginID === loginList[i].id) {
            mapLogin(loginList[i]);
            break;
        }
    }
}

function mapLogin(data) {
    var form = getUpdateForm();
    setFirstAttrByName(form, 'company', data.companyName);
    setFirstAttrByName(form, 'firstName', data.firstName);
    setFirstAttrByName(form, 'lastName', data.lastName);
    setFirstAttrByName(form, 'loginName', data.loginName);

    $(form).find('[name=accountEnabled]').prop('checked', data.accountEnabled);
    $(form).find('[name=opportunityEnabled]').prop('checked', data.opportunityEnabled);
    $(form).find('[name=partnerEnabled]').prop('checked', data.partnerEnabled);
    $(form).find('[name=portfolioEnabled]').prop('checked', data.portfolioEnabled);

    setFirstAttrByName(form, 'type', data.type);
    setFirstAttrByName(form, 'accessOverride', data.accessOverride);
}

function updateLogin() {
    var form = getUpdateForm();
    var alertTag = $(form).find('#rsStatus');
    var loginID = getFirstAttrByName(form, 'loginID');
    var type = getFirstAttrByName(form, 'type');
    var accessOverride = getFirstAttrByName(form, 'accessOverride');

    var accountEnabled = $(form).find('[name=accountEnabled]').first().prop('checked');
    var opportunityEnabled = $(form).find('[name=opportunityEnabled]').first().prop('checked');
    var partnerEnabled = $(form).find('[name=partnerEnabled]').first().prop('checked');
    var portfolioEnabled = $(form).find('[name=portfolioEnabled]').first().prop('checked');
    $.ajax({
        url: updateLogin_URL,
        type: 'POST',
        data: {loginID: loginID, type: type, accessOverride: accessOverride,
            accountEnabled: accountEnabled, opportunityEnabled: opportunityEnabled,
            partnerEnabled: partnerEnabled, portfolioEnabled: portfolioEnabled},
        success: function(data) {
            alertTag.text(data);
            alertTag.prop('color', 'green');
        }, error: function(response) {
            alertTag.text(response.responseText);
            alertTag.prop('color', 'red');
        }
    });

}