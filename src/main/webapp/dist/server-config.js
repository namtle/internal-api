$(function() {
    getServerList();
});
var serverList;
var currentServer;
function getServerCard() {
    return $('#server-card');
}

function setSCStatus(message) {
    $(getServerCard()).find('#server_err').text(message);
}
function getServerList() {
    setSCStatus('Waiting to server...');
    $.ajax({
        url: serverList_URL,
        type: 'GET',
        success: function(data) {
            serverList = data;
            currentServer = getCurrentServer();

            serverListTag = $(getServerCard()).find('[name=serverName]').first();
            serverListTag.html('');
            serverListTag.append('<option value="")></option>');
            for (i = 0; i < data.length; i++) {
                serverListTag.append('<option value="' +
                        data[i].databaseName + '"'
                        + (currentServer !== null && currentServer.databaseName === data[i].databaseName
                                ? ' selected="selected"' : '')
                        + '>' + data[i].databaseName + '</option>');
            }
            mapDBInfo();
            setSCStatus('');
        }
    });
}
function switchServer(databaseName) {
    var rs = true;
    $.ajax({
        url: switchServer_URL,
        type: 'POST',
        data: {databaseName: databaseName},
        async: false,
        success: function(data) {
            setSCStatus(data);
        }, error: function(response) {
            rs = false;
            setSCStatus(response.responseText);
        }
    });
    return rs;
}

function mapDBInfo() {
    var form = getServerCard();
    var dbName = getFirstAttrByName(form, 'serverName');
    if (dbName === '' && currentServer !== null) {
        setFirstAttrByName(form, 'serverName', currentServer.databaseName);
    } else if (currentServer !== null && dbName === currentServer.databaseName) {
        setFirstAttrByName(form, 'schema', currentServer.schema);
        setFirstAttrByName(form, 'host', currentServer.host);
        if (currentServer.port != '-1') {
            setFirstAttrByName(form, 'port', currentServer.port);
        } else {
            setFirstAttrByName(form, 'port', '');
        }
    } else if (currentServer === null || (currentServer !== null && dbName !== currentServer.databaseName)) {
        if (switchServer(dbName)) {
            for (i = 0; i < serverList.length; i++) {
                if (dbName === serverList[i].databaseName) {
                    setFirstAttrByName(form, 'schema', serverList[i].schema);
                    setFirstAttrByName(form, 'host', serverList[i].host);
                    if (serverList[i].port != '-1') {
                        setFirstAttrByName(form, 'port', serverList[i].port);
                    } else {
                        setFirstAttrByName(form, 'port', '');
                    }
                    currentServer = serverList[i];
                    break;
                }
            }
        } else {
            if (currentServer === null) {
                setFirstAttrByName(form, 'serverName', '');
            } else {
                setFirstAttrByName(form, 'serverName', currentServer.databaseName);
            }
        }
    }
}

function getCurrentServer() {
    var rs;
    $.ajax({
        url: currentServer_URL,
        type: 'GET',
        async: false,
        success: function(data) {
            rs = data;
        }, error: function(response) {
            rs = null;
        }
    });
    return rs;
}