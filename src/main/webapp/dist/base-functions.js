var getLogins_URL = 'rest/login-info/login/';
var updateLogin_URL = 'rest/login-info/update';
var companyList_URL = "rest/companies";
var teamContactKey_URL = "rest/contacts/";
var loginRoleKey_URL = "rest/login-roles/";
var userGroup_URL = "rest/user-groups/";
var createLogin_URL = "rest/login-info/create";
var updateCompanyV4_URL = "rest/companies/set-v4enabled";
var serverList_URL = 'rest/server/list';
var currentServer_URL = 'rest/server/current';
var switchServer_URL = 'rest/server/switch';
function setFirstAttrByName(holder, tagName, value) {
    $(holder).find('[name=' + tagName + ']').first().val(value);
}
function getFirstAttrByName(holder, tagName) {
    return $(holder).find('[name=' + tagName + ']').first().val();
}