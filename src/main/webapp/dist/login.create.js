$(function() {
    $(getAddLoginForm()).submit(function(e) {
        e.preventDefault();
    });
});
var teamMemberData;
function populateFromContact(contactSelect) {
    var form = getAddLoginForm();
    var key = $(contactSelect).val();
    setFirstAttrByName(form, 'firstName', '');
    setFirstAttrByName(form, 'lastName', '');
    setFirstAttrByName(form, 'shortName', '');
    setFirstAttrByName(form, 'email', '');
    if (key !== '0') {
        for (i = 0; i < teamMemberData.length; i++) {
            if (key === teamMemberData[i].id.key) {
                names = teamMemberData[i].name.split(' ');
                space = '';
                var firstName = "", lastName = "";
                for (j = 0; j < names.length - 1; j++) {
                    firstName += space + names[j];
                    space = ' ';
                }
                if (names.length > 1) {
                    lastName = names[names.length - 1];
                }
                setFirstAttrByName(form, 'firstName', firstName);
                setFirstAttrByName(form, 'lastName', lastName);
                setFirstAttrByName(form, 'shortName', teamMemberData[i].shortName);
                setFirstAttrByName(form, 'email', teamMemberData[i].email);
                break;
            }
        }
    }
}
function showMembershipOptions() {
    var form = getAddLoginForm();
    if (getFirstAttrByName(form, 'userGroupId') === '0') {
        $(form).find('.membershipRadio').hide();
    } else {
        $(form).find('.membershipRadio').show();
    }
}

function getAddLoginForm() {
    return $("#addLoginForm");
}
function disable(tag) {
    $(tag).prop("disabled", true);
}
function enable(tag) {
    $(tag).prop("disabled", false);
}
function updateCompanyList() {
    var form = getAddLoginForm();
    var alertTag = form.find("#companyID_error");
    alertTag.hide();
    $.ajax({
        url: companyList_URL,
        type: 'GET',
        success: function(data) {
            var companyList = $(form).find("[name=companyID]").first();
            disable(companyList);
            companyList.html('<option value="0" selected="selected"></option>');
            for (i = 0; i < data.length; i++) {
                companyList.append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
            }
            enable(companyList);
            setFirstAttrByName(form, 'firstName', '');
            setFirstAttrByName(form, 'lastName', '');
            setFirstAttrByName(form, 'shortName', '');
            setFirstAttrByName(form, 'email', '');
            $(form).find("[name=teamContactKey]").first().html('<option value="-1" selected="selected"></option><option value="0">Create New</option>');
            $(form).find("[name=loginRoleKey]").first().html('<option value="0" selected="selected"></option>');
            $(form).find("[name=userGroupId]").first().html('<option value="0" selected="selected"></option>');
        },
        error: function(response) {
            alertTag.text(response.responseText);
            alertTag.show();
        }
    });
}

function changeCompany() {
    var form = getAddLoginForm();
    var companyID = $(form).find("[name=companyID]").first().val();
    if (companyID !== '0') {
        updateTeamMemberList(companyID);
        updateAccessRoleList(companyID);
        updateUserGroupList(companyID);
    }
    setFirstAttrByName(form, 'firstName', '');
    setFirstAttrByName(form, 'lastName', '');
    setFirstAttrByName(form, 'shortName', '');
    setFirstAttrByName(form, 'email', '');
    $(form).find('.membershipRadio').hide();
    $(form).find('rsStatus').hide();
}

function updateTeamMemberList(companyID) {
    var form = getAddLoginForm();
    $.ajax({
        url: teamContactKey_URL + companyID,
        type: 'GET',
        success: function(data) {
            teamMemberData = data;
            var contactList = $(form).find("[name=teamContactKey]").first();
            contactList.html('<option value="-1" selected="selected"></option><option value="0">Create New</option>');
            for (i = 0; i < data.length; i++) {
                contactList.append('<option value="' + data[i].id.key + '">' + data[i].name + '</option>');
            }
        }
    });
}

function updateAccessRoleList(companyID) {
    var form = getAddLoginForm();
    $.ajax({
        url: loginRoleKey_URL + companyID,
        type: 'GET',
        success: function(data) {
            var accessRoleList = $(form).find("[name=loginRoleKey]").first();
            accessRoleList.html('<option value="0" selected="selected"></option>');
            for (i = 0; i < data.length; i++) {
                accessRoleList.append('<option value="' + data[i].id.key + '">' + data[i].name + '</option>');
            }
        }
    });
}
function updateUserGroupList(companyID) {
    var form = getAddLoginForm();
    $.ajax({
        url: userGroup_URL + companyID,
        type: 'GET',
        success: function(data) {
            var userGroupList = $(form).find("[name=userGroupId]").first();
            userGroupList.html('<option value="0" selected="selected"></option>');
            for (i = 0; i < data.length; i++) {
                userGroupList.append('<option value="' + data[i].id + '">' + data[i].groupName + '</option>');
            }
        }
    });
}
function submitAddForm() {
    var form = getAddLoginForm();
    var alertTag = $(getAddLoginForm()).find('#rsStatus');
    alertTag.html('');
    var login = {};
    login.companyId = getFirstAttrByName(form, 'companyID');
    login.loginName = getFirstAttrByName(form, 'loginName');
    login.password = getFirstAttrByName(form, 'password');
    login.firstName = getFirstAttrByName(form, 'firstName');
    login.lastName = getFirstAttrByName(form, 'lastName');
    login.shortName = getFirstAttrByName(form, 'shortName');
    login.email = getFirstAttrByName(form, 'email');

    login.accountEnabled = $(form).find('[name=accountEnabled]').first().prop('checked');
    login.opportunityEnabled = $(form).find('[name=opportunityEnabled]').first().prop('checked');
    login.partnerEnabled = $(form).find('[name=partnerEnabled]').first().prop('checked');
    login.portfolioEnabled = $(form).find('[name=portfolioEnabled]').first().prop('checked');
    login.accessOverride = getFirstAttrByName(form, 'accessOverride');
    login.type = getFirstAttrByName(form, 'type');
    login.loginRoleKey = getFirstAttrByName(form, 'loginRoleKey');
    var teamContactKey = getFirstAttrByName(form, 'teamContactKey');
    var userGroupId = getFirstAttrByName(form, 'userGroupId');
    var membershipType = $(getAddLoginForm()).find('input[name=membershipType]:checked').val();
    var validateRs = validate(login);
    if (validateRs !== '') {
        alertTag.html('<pre>' + validateRs + '</pre>');
        alertTag.prop('color', 'red');
    } else
        $.ajax({
            url: createLogin_URL,
            type: 'POST',
            data: JSON.stringify({login: login, teamContactKey: teamContactKey, userGroupId: userGroupId, membershipType: membershipType}),
            contentType: "application/json",
            dataType: 'json',
            success: function(data) {
                alertTag.text(data);
                alertTag.prop('color', 'green');
            }, error: function(response) {
                if (response.responseText !== '') {
                    alertTag.text(response.responseText);
                } else
                    alertTag.text('Something went wrong! Please try again!');
                alertTag.prop('color', 'red');
            }
        });
}

function validate(login) {
    var emailRegex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    var emptyError = 'Please fill ';
    var rs = '';
    if (login.loginName.trim() === '') {
        rs += emptyError + 'username\n';
    }
    if (login.password.trim() === '') {
        rs += emptyError + 'password\n';
    }
    if (login.firstName.trim() === '') {
        rs += emptyError + 'first name\n';
    }
    if (login.lastName.trim() === '') {
        rs += emptyError + 'last name\n';
    }
    if (login.shortName.trim() === '') {
        rs += emptyError + 'short name\n';
    }
    if (login.email.trim() === '') {
        rs += emptyError + 'email\n';
    } else if (!login.email.trim().match(emailRegex)) {
        rs += 'Wrong email format';
    }
    return rs;
}
